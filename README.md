PeanutsJS
=========

Need to rewrite the whole damn thing. Need better code.

Javascript Framework

--

Methods:<br />

<br />
css(property, value)<br />
Set css:<br />
Example: peanuts('#id').css('background', 'red'); <br />
<br />
Get css:<br />
Example: peanuts('#id').css('background') or peanuts('#id').css(['background', 'color', 'width'])
<br /><br /><br />

Get/set Attribute:<br />
Example: peanuts('#id').attr('type') or set attribute peanuts('#id').attr('type', 'button')
<br /><br /><br />

Append child:<br />
Example: peanuts('#id').append('your text', 'li') or peanuts('#id').append(['text one', 'text two', 'text three'], 'li')
<br /> <br /> <br />

Bind event listener: <br />
Example: peanuts('#id').bind('click', function(){})
<br /> <br /> <br />

Get or Set text from/to a node: <br />
Example: peanuts('#id').text() for return. peanuts('#id').text('set text to node')
<br /> <br /> <br />

Get select/input value: <br />
Example: peanuts('#id').val()
Todo: set value <br />
<br /> <br /> <br />

Add class:<br />
Example: peanuts('#id').addClass('classname')
<br /> <br /> <br />

Has class:<br />
Example: peanuts('#id').hasClass('classname')
<br /> <br /> <br />

Remove class:<br />
Example: peanuts('#id').removeClass('classname')
<br /> <br /> <br />

Hide node:<br />
Example: peanuts('#id').hide()
<br /> <br /> <br />

Show node:<br />
Example: peanuts('#id').show()
<br /> <br /> <br />

Remove node:<br />
Example: peanuts('#id').remove()
<br /> <br /> <br />

Get position (left, right, top, bottom, height, width):<br />
Example: peanuts('#id').pos() 
<br /> <br /> <br />