/*! Peanuts JS Library 
* * http://www.workbench.no/blog/
* * https://github.com/bjarneo/PeanutsJS
* * Copyright (c) 2013 Bjarne �verli; Licensed MIT 
* * IE < 8 won't work 
* */
(function() {
	
	var p = {
		/**
		 * inArray function as PHP
		 * @param String val (needle)
		 * @param Array arr (haystack)
		 */
		inArray: function(val, arr) {
			var i = 0,
				len = arr.length;
			while(len--) {
				if(val === arr[len]) {
					return true;
				}
			}
			return false;
		},
		
		/**
		 * Object to Array
		 * @param Object obj
		 */
		objToArray: function(obj) {
			var arr = [],
				len = obj.length;
			while(len--) {
				arr[len] = obj[len]; 
			}
			
			return arr;
		},
		
		/**
		 * Array to Object
		 * @param Array arr
		 */
		arrToObject: function(arr) {
			var obj = {},
				len = arr.length;
			while(len--) {
				obj[len] = arr[len]
			}
			
			return obj;
		}, 
		
		/**
		 * Add event
		 * @param elem (selector)
		 * @param listener
		 * @param function
		 */
		addEvent: function(elem, listener, func) {
			
			if(elem.addEventListener) {
				elem.addEventListener(listener, func, false);
			} else {
				elem.attachEvent('on' + listener, func);
			}
			return elem;
		}, 
		
		log: function() {
			if(this.console){
			  console.log( Array.prototype.slice.call(arguments) );
			}
		},

		/**
		 * Ajax
		 * @param params (object) data,type,url,dataType,success
		 * @todo TONS
		 */
		ajax: function(params) {
			var request = new XMLHttpRequest(),
				type;
			
			if(params.data) {
				// loop through data properties and set to url.
				//request.open(params.type, params.url))
			} else {
				request.open(params.type, params.url);	
			}
			console.log(params.type + params.url);
			request.onreadystatechange = function () {
				if(request.readyState === 4 && request.status === 200) {
					if(params.dataType) {
						type = request.getResponseHeader('Content-type', 'application/' + params.dataType + ';charset=UTF-8')
					} else {
						type = request.getResponseHeader('Content-type');
					}
					if(typeof params.success === 'function') {
						if(type.indexOf('xml') !== -1 && request.responseXML) {
							params.success(request.responseXML);
						} else if (type === 'application/json') {
							params.success(JSON.parse(request.responseText));
						} else {
							params.success(request.responseText);
						}
						
					}
				}
			};	
			
			request.send(null);
		}
	}
	
	peanuts = function(params) {
		return new Peanuts(params);
	};
	
	var Peanuts = function(params) {
		var selector = document.querySelectorAll(params),
			len = selector.length,
			about = {
				Version: 0.1,
				Author: 'Bjarne Oeverli',
				Website: 'http://www.workbench.no/',
				GitHub: 'https://github.com/bjarneo/PeanutsJS',
				Created: '05.02.2013',
				Updated: '14.06.2013'
		};
		
		for(var i = 0; i < len; i++) {
			this[i] = selector[i];
		}
				
		if(console) {
			console.log(p.objToArray(selector));
		}
		
		this.length = selector.length;
		
		this.about = about;
		
		return this;
	}

	peanuts.fn = Peanuts.prototype = {
		init: function() {
			return this;
		},

		/**
		 * Set css property and value. Ex: 'background', 'red'
		 * Get css property (array or string)
		 * @param String property
		 * @param String value
		 * @support Doesn't support < IE9 (getComputedStyle)
		 */
		css: function(property, value) {
			if(value == null) {
				var i = property.length,
					list = [];
				if(property instanceof Array) {
					while(i--) {
						list[i] = document.defaultView.getComputedStyle(this[0], null).getPropertyValue(property[i]);
					}
					return list;
				} else {
					return document.defaultView.getComputedStyle(this[0], null).getPropertyValue(property);
				}
			} else {
				var len = this.length;
				while(len--) {
					this[len].style[property] = value;
				}
			}
			return this;
		},
		
		/**
		 * Append Child
		 * @param String/Array text
		 * @param String node
		 */
		append: function(text, node) {
			try {
				if(text instanceof Array) {
					var i = text.length,
						j = text.length;
					var nodeArray = [];
					while(i--) {
						nodeArray.push(node);
					}
					while(j--) {
						var node = document.createElement(nodeArray[j]);
						node.innerHTML = text[j];
						this[0].appendChild(node);
					}
				} else {
					var node = document.createElement(node);
					node.innerHTML = text;
					this[0].appendChild(node);
				}
				
				return this;
			} catch(e) {
				e.message;
			}
		},

		/**
		 * Get node attributes
		 * @param String attribute
		 * @param String value
		 */
		attr: function(attr, value) {
			var attribute,
				list = [],
				len = this.length,
				i = 0;

			if(!value) {
				for(; i < len; i++) {
					list[i] = this[i].getAttribute(attr);
				}
				
				return list;
			} else {
				for(; i < len; i++) {
					this[i].setAttribute(attr, value);
				} 

				return this;
			}
		},
		
		/**
		 * Bind Event Listener
		 * @param Listener listener
		 * @param Function func
		 * @todo add array also
		 */
		bind: function(listener, func = false) {
			var i = this.length;

			if(listener) {
				while(i--) {
					p.addEvent(this[i], listener, func);
				} 
			} else {
				throw new Error('Couldn\'t find the listener you were looking for.');	
			}
			
			return this;
		},
		
		/**
		 * Get/set text to node.
		 * @param String text
		 */
		text: function(text) {
			var i = this.length;
				
			if(typeof text == 'string') {
				while(i--) {
					this[i].innerHTML = text;
				}
				return this;
			} else {
				while(i--) {
					return this[i].innerHTML;
				}
			}
		},
		
		/**
		 * Set/Get input value
		 * @param value
		 */
		val: function(value) {
			var len = this.length,
				list = [];
			
			if(!value) {
				while(len--) {
					list[len] = this[len].value;
				}
				return list;
			} else {
				while(len--) {
					this[len].value = value;
				}

				return this;
			}
		},
		
		/**
		 * Add class
		 * @param String clName
		 */
		addClass: function(clName) {
			var len = this.length;
			
			if(!this.hasClass(clName)) {
				while(len--) {
					this[len].className += ' ' + clName; 
				}	
			} 
			
			return this;
		},
		
		/**
		 * Has class
		 * @param String clName
		 */
		hasClass: function(clName) {
			var len = this.length,
				regex = new RegExp('(^|\\s)' + clName + '(\\s|$)');
				
			return regex.test(this[0].className); 
		},
		
		/**
		 * Remove class
		 * @param String class
		 * @todo fix regex
		 */
		removeClass: function(clName) {
			var len = this.length,
				//r = '/(?:^|\s)' + clName + '(?!\S)/g';
				regex = new RegExp('(^|\\s)' + clName + '(\\s|$)');

			while(len--) {
				this[len].className = this[len].className.replace(regex, ' '); 
			}
			
			return this;				
		},
		
		/**
		 * Hide node(s)
		 */
		hide: function() {
			var len = this.length;
			while(len--) {
				this[len].style.display = 'none';
			}
			return this;
		},
		
		/**
		 * Show node(s)
		 */
		show: function() {
			var len = this.length;
			while(len--) {
				this[len].style.display = 'block';
			}
			return this;
		},

		/**
		 * Remove node(s)
		 */
		remove: function() {
			var len = this.length;
			while(len--) {
				this[len].parentNode.removeChild(this[len]);
			}
			return this;
		},
		
		/**
		 * Check browser
		 */
		browser: function() {
			var browser = navigator;
			return browser;
		},
		
		/**
		 * Get current positions (left,right,top,bottom,height,width)
		 */
		pos: function() {
			var list = [],
				len = this.length;
			
			while(len--) {
				list[len] = this[len].getBoundingClientRect();
			}
			
			return list;
		},

		/**
		 * Animate
		 */
		animate: function() {
			
		},
		
		/**
		 *  How to add a function to method.
		 */
		add: function(func) {
			if(typeof func === 'function') {
				func.call(this);
			}
			
			return this;
		}
	};
	
	/**
	 * Extend current class with ajax. 
	 */
	peanuts.fn.ajax = function() {
		alert('ajax');
	}

	if(!window.p || !window.peanuts) {
		(window.p = p) && (window.peanuts = peanuts);
	}
})();